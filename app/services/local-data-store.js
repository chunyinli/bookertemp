import Ember from 'ember';

export default Ember.Service.extend({
    data: null,
    lastId: 0,
    init() {
        this._super(...arguments);

        this.set('data', JSON.parse(window.localStorage.getItem('todosData') || '[]'));
        this.set('lastId', window.localStorage.getItem('todosLastId') || 0);
    },
    reset() {
      this.get('data').clear();
      this.set('lastId', 0);
    },
    findAll() {
      console.log('findAll');
        return this.get('data') ||
        this.set('data', JSON.parse(window.localStorage.getItem('todosData') || '[]'));
    },
    add(attrs) {
      console.log(this.get('lastId'));
      let todo = Object.assign({ id: this.incrementProperty('lastId') }, attrs);

      this.get('data').pushObject(todo);
      this.persist();
      return todo;
    },
    toggleComplete(id) {
      let allTodos = this.get('data');
      let todoIndex = allTodos.findIndex(i => i.id === id);
      let todo = allTodos.objectAt(todoIndex);

      todo.completed = !todo.completed;
      // todo.set('completed', !todo.completed);
      // todo.toggleProperty('completed');

      allTodos.replace(todoIndex, 1, todo);

      this.persist();
      return allTodos;
    },
    removeAll() {
      this.reset();
      this.persist();
    },
    persist() {
        window.localStorage.setItem('todosData', JSON.stringify(this.get('data')));
        window.localStorage.setItem('todosLastId', this.get('lastId'));
    }
});
