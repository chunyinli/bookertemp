import Ember from 'ember';

export default Ember.Route.extend({
    model() {
        return this.get('localDataStore').findAll();
    },
    localDataStore: Ember.inject.service('local-data-store')
});
