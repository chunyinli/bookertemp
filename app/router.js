import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('sisco');
  this.route('archived', function() {
    this.route('sub-route');
  });
});

export default Router;
