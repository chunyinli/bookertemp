import Ember from 'ember';

export default Ember.Component.extend({
  localDataStore: Ember.inject.service('local-data-store'),
  actions: {
    toggleComplete(newValue) {
      console.log(newValue);
      let todoId = this.get('todo.id');
      this.get('localDataStore').toggleComplete(todoId);
    }
  },
  test: Ember.computed(() => {
    console.log(this);
    // debugger;
    return true
  })
});
